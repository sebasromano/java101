package com.books;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;



public class User {
	
	private int id;
	private String username;
	private String userpass;
	
	private RoleData roleData = new RoleData();
	
	// almacenar historial de acciones del usuario
	private List<UserHistory> history = new ArrayList<UserHistory>();
	

	/**
	 * @return the userHistory
	 */
	public List<UserHistory> getHistory() {
		return history;
	}
	/**
	 * @param userHistory the userHistory to set
	 */
	public void setHistory(List<UserHistory> userHistory) {
		this.history = userHistory;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the userpass
	 */
	public String getUserpass() {
		return userpass;
	}
	/**
	 * @param userpass the userpass to set
	 */
	public void setUserpass(String userpass) {
		this.userpass = userpass;
	}
	
	/**
	 * @return the roleData
	 */
	public RoleData getRoleData() {
		return roleData;
	}
	/**
	 * @param roleData the roleData to set
	 */
	public void setRoleData(RoleData roleData) {
		this.roleData = roleData;
	}	
	
	public void addHistory( UserHistory historyItem)
	{
		historyItem.setUser(this);
		history.add(historyItem);
		
	}

}
