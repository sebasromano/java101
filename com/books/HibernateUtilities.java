package com.books;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtilities {
	private static SessionFactory sessionFactory;
	static{
		try{
			// trata de tomar la configuracion
			Configuration conf = new Configuration().setInterceptor( new UserInterceptor() ).configure();
			sessionFactory = conf.buildSessionFactory();
			StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(conf.getProperties());
	        sessionFactory = conf.buildSessionFactory(ssrb.build());			
		}
		catch( HibernateException exception)
		{
			System.out.println("Problema creando sesi�n");
		}
	}
	
	public static SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
}
