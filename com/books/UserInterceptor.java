package com.books;

import java.io.Serializable;
import java.util.Iterator;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

public class UserInterceptor extends EmptyInterceptor {

	/* (non-Javadoc)
	 * @see org.hibernate.EmptyInterceptor#onSave(java.lang.Object, java.io.Serializable, java.lang.Object[], java.lang.String[], org.hibernate.type.Type[])
	 */
	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		// TODO Auto-generated method stub
		System.out.println("Guardando entidad");
		return super.onSave(entity, id, state, propertyNames, types);
	}

	/* (non-Javadoc)
	 * @see org.hibernate.EmptyInterceptor#postFlush(java.util.Iterator)
	 */
	@Override
	public void postFlush(Iterator entities) {
		// TODO Auto-generated method stub
		System.out.println("Entidad flushed");
		super.postFlush(entities);
	}

}
