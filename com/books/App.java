package com.books;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hola mundo");
		Session  session = HibernateUtilities.getSessionFactory().openSession();
		
		// comienzo la transacción
		session.beginTransaction();
		
		// creo el usuario
		User admin = new User();
		admin.setUsername("admin");
		admin.setUserpass("admin");
		admin.getRoleData().setRole("ADMIN");
		admin.addHistory( new UserHistory( new Date(), "Usuario creado") );
		session.save(admin);
		
		User user1 = new User();
		user1.setUsername("root");
		user1.setUserpass("root");
		user1.getRoleData().setRole("ADMIN");
		user1.addHistory( new UserHistory( new Date(), "Usuario root creado") );
		session.save(user1);
		
		User test = new User();
		test.setUsername("test");
		test.setUserpass("test");
		test.getRoleData().setRole("USER");
		test.addHistory( new UserHistory( new Date(), "Usuario test creado") );
		session.save(test);
		
		// trata de hacer el commit
		session.getTransaction().commit();
		
		// mostrar un usuario
		session.beginTransaction();
		User loadedUser = (User) session.load( User.class, 1);
		System.out.println( loadedUser.getUsername());
		session.getTransaction().commit();
		
		// query created users (HQL);
		session.beginTransaction();
		Query query = session.createQuery("from User");
		List<User> users = query.list();
		for( User user: users)
		{
			System.out.println( user.getUsername());	
		}
		session.getTransaction().commit();
		
		// Criteria
		session.beginTransaction();
		Criteria criteria = session.createCriteria( User.class);
		criteria.add(Restrictions.eq("username", "admin"));
		List<User> userlist = criteria.list();
		for( User user: userlist)
		{
			System.out.println( "Criteria " + user.getUsername());	
		}
		session.getTransaction().commit();
		
		// buscando por example
		/*User exampleUser = new User();
		exampleUser.setUsername("test");
		Example e = Example.create( exampleUser).ignoreCase();
		Criteria criteria2 = session.createCriteria( User.class);
		criteria2.add( e);
		List<User> userlist2 = criteria2.list();
		for( User user: userlist2)
		{
			System.out.println( "Criteria plus Example: " + user.getUsername());	
		}
		session.getTransaction().commit();		
		
		// batch update
		Query updateQuery = session.createQuery("update User user set user.userpass = 'admin'");
		updateQuery.executeUpdate();
		session.getTransaction().commit();	*/	
		
		session.close();
		HibernateUtilities.getSessionFactory().close();
	}

}
